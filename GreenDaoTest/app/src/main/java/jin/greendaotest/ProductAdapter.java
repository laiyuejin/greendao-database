package jin.greendaotest;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class ProductAdapter extends AbsRecyclerViewAdapter {
    public List<Product> datas = new ArrayList<>();
    private Context context;

    public ProductAdapter(final Context activity, RecyclerView recyclerView) {
        super(recyclerView);
        this.context = context;
    }

    public void updateItems(List<Product> addData) {
        if (addData.size() == 0)
            return;
//        int size = datas.size();
        datas.addAll(addData);
//        notifyDataSetChanged() is called in AbsRecyclerViewAdapter line 28

    }

    @Override
    public ClickableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        bindContext(parent.getContext());
        return new ItemViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_note, parent, false));
    }

    public Product getNote(int p) {
        return datas.get(p);
    }

    @Override
    public void onBindViewHolder(ClickableViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder) {
            final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            itemViewHolder.name.setText("name:"+datas.get(position).getName());
            itemViewHolder.price.setText("price:"+datas.get(position).getPrice());
        }
        super.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemCount() {
        return datas == null ? 0 : datas.size();
    }

    public class ItemViewHolder extends AbsRecyclerViewAdapter.ClickableViewHolder {

        public TextView name;
        public TextView price;
        public View item;

        public ItemViewHolder(View itemView) {
            super(itemView);
            item = itemView;
            name = $(R.id.name);
            price = $(R.id.price);
        }
    }
}
