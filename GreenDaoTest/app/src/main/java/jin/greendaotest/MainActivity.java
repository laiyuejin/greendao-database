package jin.greendaotest;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.socks.library.KLog;
import org.greenrobot.greendao.query.Query;
import java.util.List;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    private Button addButton;
    private ProductDao productDao;
    private Query<Product> productQuery;
    private ProductAdapter adapter;
    private StaggeredGridLayoutManager mLayoutManager;
    int mPage = 0;
    final int pageSize = 20;
    boolean isLoading = false;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        context = this;
        setUpViews();
        // get the note DAO
        final DaoSession daoSession = ((App) getApplication()).getDaoSession();
        productDao = daoSession.getProductDao();
        loadData();
    }

    protected void setUpViews() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerViewNotes);
        //noinspection ConstantConditions
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new ProductAdapter(this, recyclerView);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(OnLoadMoreListener());
        addButton = (Button) findViewById(R.id.buttonAdd);
    }

    public void onAddButtonClick(View view) {
        addProducts();
    }

    //scroll to judge if recyclerView touch the bottom
    RecyclerView.OnScrollListener OnLoadMoreListener() {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView rv, int dx, int dy) {
                boolean isBottom = mLayoutManager.findLastCompletelyVisibleItemPositions(
                        new int[2])[1] >= adapter.getItemCount() - pageSize;
                if (isBottom && !isLoading) {  //avoid load 2 times
                    isLoading = true;
                    loadData();
                }
            }
        };
    }

    public void loadData() {
        productQuery = productDao.queryBuilder().orderAsc(ProductDao.Properties.Id).offset(mPage * pageSize).limit(pageSize).build();
        long start = System.currentTimeMillis();
        List<Product> data = productQuery.list();
        KLog.e("Time used to insert: " + (System.currentTimeMillis()-start));
        adapter.updateItems(data);
        isLoading = false;
        if(data.size()>0)
             mPage++;
    }

    private void addProducts() {
//        Thread t = new Thread() {
//            public void run() {
//                for (int i = 20; i < 10000; i++) {
//                    Product item = new Product(null, "product" + i, "" + i);
//                    productDao.insert(item);
//                }
//            }
//        };
//        t.start();
        addButton.setEnabled(false);
        addButton.setText("Loading");
        Toast.makeText(this, "start insert data", Toast.LENGTH_LONG).show();

        //Used Rxandroid library
        Observable ob = Observable.create(new Observable.OnSubscribe<Long>() {

            @Override
            public void call(Subscriber<? super Long> subscriber) {
                Long start = System.currentTimeMillis();
                for (int i = 0; i < 1000; i++) {
                    Product item = new Product(null, "product" + i, "" + i);
                    productDao.insert(item);
                }
                Long timeUsed = System.currentTimeMillis() - start;
                subscriber.onNext(timeUsed);
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        Action1<Long> onNext = new Action1<Long>() {
            @Override
            public void call(Long aLong) {
                addButton.setText("Used Time:"+aLong);
                KLog.e("Time used to insert: " + aLong);
                Toast.makeText(context, "Time used to insert:" + aLong, Toast.LENGTH_LONG).show();
                loadData();
                adapter.notifyDataSetChanged();
            }
        };
        ob.subscribe(onNext);

    }
}
